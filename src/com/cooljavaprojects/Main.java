package com.cooljavaprojects;

public class Main {

    //Polymorphism: this interface is going to be implemented in different classes
    //and in each class method hello will have it's own meaning
    interface Greeting {
        void hello();
    }

    static class Employer implements Greeting {
        Employer() {
        }

        public void hello() {
            System.out.println("employer: hi! introduce yourself and describe your java experience please");
        }
    }

    abstract static class Candidate implements Greeting {
        String name;

        //Encapsulation: access to the class data (name)
        //is realised through the constructors and methods of the Candidate class.
        //The complexity of realisation of those constructors and methods is hidden inside the class
        //and not visible when using them.
        Candidate(String s) {
            name = s;
        }

        public void hello() {
            System.out.println("candidate: hi! my name is " + name + "!");
        }

        //Polymorphism: this method is going to be inherited by two different classes
        //and in each class this method will have it's own meaning
        abstract void describeExperience();
    }

    //Inheritance: GetJavaJobGraduate class acquires the properties of Candidate class.
    //It has same methods and data fields.
    //GetJavaJobGraduate also inherits encapsulation from Candidate.
    static class GetJavaJobGraduate extends Candidate {
        GetJavaJobGraduate(String s) {
            super(s);
        }

        void describeExperience() {
            System.out.println("candidate: I passed successfully getJavaJob exams and code reviews.\n");
        }
    }

    //Inheritance: SelfLearner class acquires the properties of Candidate class.
    //It has same methods and data fields.
    //SelfLearner also inherits encapsulation from Candidate.
    static class SelfLearner extends Candidate {
        SelfLearner(String s) {
            super(s);
        }

        void describeExperience() {
            System.out.println("candidate: I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code.\n");
        }
    }

    public static void main(String[] args) {
        Employer employer = new Employer();
        Candidate[] candidates = new Candidate[10];
        for (int i = 0; i < 3; i++) {
            candidates[i] = new GetJavaJobGraduate("name" + i);
        }
        for (int i = 3; i < 10; i++) {
            candidates[i] = new SelfLearner("name" + i);
        }
        for (Candidate candidate : candidates) {
            employer.hello();
            candidate.hello();
            candidate.describeExperience();
        }
    }
}
